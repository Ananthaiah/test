// pipeline-library/vars/commonPipeline.groovy

def call(String branchName = 'default', boolean updateArgoCD = false) {
    pipeline {
        agent any

        environment {
            buildnumber = "${env.BUILD_NUMBER}"
            branchname = branchName
        }

        tools {
            jdk 'JAVA_HOME'
            nodejs 'nodejs'
        }

        stages {
            stage('Git-clone') {
                steps{
			 sh '''
                   echo "This is working"
                   '''
		}
            }
        }
    }
}
